from django.http import JsonResponse
from common.json import ModelEncoder
from .models import AccountVO, Attendee, ConferenceVO
from django.views.decorators.http import require_http_methods
import json

class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
    ]

class AttendeeDetailsEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder()
    }
    def get_extra_data(self, o):
        count = len(AccountVO.objects.filter(email=o.email))
        return {"has_account": count > 0}

@require_http_methods(["GET","POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse({"attendees": attendees}, encoder=AttendeeListEncoder,)
    else:
        content = json.loads(request.body)
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse({"message": "Invalid conference id"}, status=400,)
    attendee = Attendee.objects.create(**content)
    return JsonResponse(attendee, encoder=AttendeeDetailsEncoder, safe=False,)

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, pk):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse({"attendee": attendee}, encoder=AttendeeDetailsEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(id=pk)
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse({"message": "Invalid conference id"}, status=400,)
    Attendee.objects.filter(id=pk).update(**content)
    attendee = Attendee.objects.get(id=pk)
    return JsonResponse(attendee, encoder=AttendeeDetailsEncoder, safe=False,)