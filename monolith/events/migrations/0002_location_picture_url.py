# Generated by Django 4.1 on 2022-08-18 20:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("events", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="location",
            name="picture_url",
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
