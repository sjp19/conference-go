from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation, Status
from django.views.decorators.http import require_http_methods
from events.api_views import ConferenceListEncoder
import json, pika

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"status": o.status.name}

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}

# class PresentationDetailEncoder(ModelEncoder):
#     model = Presentation
#     properties = [
#         "presenter_name",
#         "company_name",
#         "presenter_email",
#         "title",
#         "synopsis",
#         "created",
#     ]

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentation = Presentation.objects.all()
        return JsonResponse({"presentation": presentation}, encoder = PresentationListEncoder)
    else:
        content = json.loads(request.body)
        try: 
            status = Status.objects.filter(id=conference_id)
            content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse({"status": "Invalid Status"}, status=400)
        presentation = Presentation.create(**content)
        return JsonResponse(presentation, encoder=PresentationDetailEncoder, safe=False,)

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, pk):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=pk)
        return JsonResponse(presentation, encoder=PresentationDetailEncoder, safe=False,)
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})
    else: 
        content = json.loads(request.body)
    Presentation.objects.filter(id=pk).update(**content)
    presentation = Presentation.objects.get(id=pk)
    return JsonResponse(presentation, encoder=PresentationDetailEncoder, safe=False)

def producer(message, queue_name):
    message_json = json.dumps(message)
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=message_json,
    )
    connection.close()

@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.approve()
    message = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    queue_name = "presentation_approvals"
    producer(message, queue_name)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )

@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.reject()
    message = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    queue_name = "presentation_rejections"
    producer(message, queue_name)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )